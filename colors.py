from typing import Tuple, Union


colors = [
    "#800000",
    "#006400",
    "#808000",
    "#483d8b",
    "#008b8b",
    "#cd853f",
    "#000080",
    "#32cd32",
    "#8fbc8f",
    "#800080",
    "#b03060",
    "#ff4500",
    "#ff8c00",
    "#00ff00",
    "#9400d3",
    "#dc143c",
    "#00ffff",
    "#00bfff",
    "#0000ff",
    "#adff2f",
    "#da70d6",
    "#ff00ff",
    "#1e90ff",
    "#fa8072",
    "#ffff54",
    "#b0e0e6",
    "#90ee90",
    "#ff1493",
    "#7b68ee",
    "#ffdead",
    "#ffb6c1",
    "#808080",
]


def hex_color_to_rgb(hex_color: str) -> Tuple[float, float, float]:
    """
    :param hex_color: color as a hex. assumed to be of the form : 
        '#[0-9]{6}'
    """
    return (
        int(hex_color[1:3], 16) / 255,
        int(hex_color[3:5], 16) / 255,
        int(hex_color[5:7], 16) / 255,
    )


def rgb_to_hex_color(rgb: Tuple[float, float, float]) -> str:
    def component_to_hex(component: float) -> str:
        hex_value = hex(int(component * 255))[2:]
        if len(hex_value) == 1:
            hex_value = "0" + hex_value
        return hex_value

    return "".join(
        [
            "#",
            component_to_hex(rgb[0]),
            component_to_hex(rgb[1]),
            component_to_hex(rgb[2]),
        ]
    )


def yellow_to_red_gradient(
    gradient: float, format: str = "hex"
) -> Union[str, Tuple[float, float, float]]:
    """
    :param gradient: a float between 0 and 1
    :param format: str, one of ('rgb', 'hex')
    """
    assert format in ("rgb", "hex")
    assert gradient >= 0
    assert gradient <= 1

    rgb = (1.0, 1.0 - gradient, 0)

    if format == "rgb":
        return rgb
    return rgb_to_hex_color(rgb)


def yellow_to_red_gradient_hex(gradient: float) -> str:
    return yellow_to_red_gradient(gradient, format="hex")


def yellow_to_red_gradient_rgb(gradient: float) -> Tuple[int, int, int]:
    return yellow_to_red_gradient(gradient, format="rgb")
