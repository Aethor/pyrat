# PyRAT : Python Relationships Analysis Tool

PyRAT is a tool for interactive visualisation of dynamic character networks.

Find the documentation at https://aethor.gitlab.io/pyrat/


# Setup

- `pip install -r requirements.txt` will install the required python packages
- `Gtk` and `PyGObject` are needed. See instructions here for installation : https://pygobject.readthedocs.io/en/latest/getting_started.html
- Launch the software with `python3 pyrat.py`
