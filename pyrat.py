from colors import hex_color_to_rgb
from ui import SelectFromListDialog
import math
import time
import traceback
from typing import Optional, Tuple, Dict, List

from colors import colors
from utils import clamp
from simulation.simulation import EventStep, Simulation, Analyse
from filters import Filter, all_filters
from lenses import Lens, all_lenses

import cairo
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk


class Camera:

    CHARACTER_RADIUS = 5
    MAX_LINE_WIDTH = 5
    MIN_LINE_WIDTH = 0.5

    def __init__(self, position: Tuple[float, float]) -> None:
        self.position = position
        self.zoom = 1
        self.lens: Optional[Lens] = None

    def draw(self, simulation: Simulation, cairo_context: cairo.Context):

        characters = simulation.filtered_characters()
        relations = simulation.filtered_relations()

        # graph edges
        for relation in relations:

            # set edge color depending on relationship
            if relation.is_positive():
                cairo_context.set_source_rgb(0, 255, 0)
            else:
                cairo_context.set_source_rgb(255, 0, 0)

            # line width
            cairo_context.set_line_width(
                min(
                    max(abs(relation.polarity), Camera.MIN_LINE_WIDTH),
                    Camera.MAX_LINE_WIDTH,
                )
            )

            # adjust edge position to account for zoom
            character1_position = self.zoom * relation.character1.position
            character2_position = self.zoom * relation.character2.position

            # draw edge
            cairo_context.move_to(
                character1_position[0] - self.position[0],
                character1_position[1] - self.position[1],
            )
            cairo_context.line_to(
                character2_position[0] - self.position[0],
                character2_position[1] - self.position[1],
            )
            cairo_context.stroke()

        # graph nodes
        # colors if needed
        character_to_color: Optional[Dict[str, str]] = None
        if not self.lens is None:
            character_to_color = self.lens.apply(simulation)

        for character in characters:

            # set character color
            if not character_to_color is None:
                cairo_context.set_source_rgb(
                    *hex_color_to_rgb(character_to_color.get(character.name, colors[0]))
                )
            else:
                cairo_context.set_source_rgb(0, 0, 0)

            # adjust character position to account for zoom
            character_position = self.zoom * character.position

            # draw character circle
            cairo_context.arc(
                character_position[0] - self.position[0],
                character_position[1] - self.position[1],
                Camera.CHARACTER_RADIUS,
                0,
                2 * math.pi,
            )
            cairo_context.fill()

            # draw character name
            cairo_context.set_source_rgb(0, 0, 0)
            cairo_context.move_to(
                character_position[0] - self.position[0] + Camera.CHARACTER_RADIUS,
                character_position[1] - self.position[1],
            )
            cairo_context.show_text(character.name)

    def add_offset(self, x: float, y: float):
        """
        Add an offset to the camera position
        """
        self.position = (self.position[0] + x, self.position[1] + y)


class PyRAT:
    def __init__(self) -> None:

        self.builder = Gtk.Builder.new_from_file("./pyrat.glade")

        # Main window
        self.window: Gtk.Window = self.builder.get_object("main_window")
        self.window.connect("destroy", Gtk.main_quit)

        # Menu items
        self.menu_quit: Gtk.MenuItem = self.builder.get_object("menu_quit")
        self.menu_quit.connect("activate", Gtk.main_quit)

        self.menu_file: Gtk.MenuItem = self.builder.get_object("menu_file")
        self.menu_file.connect("activate", self.on_open_file)

        # Canvas
        self.canvas: Gtk.DrawingArea = self.builder.get_object("canvas")

        ## canvas press signals
        self.canvas.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self.canvas.add_events(Gdk.EventMask.POINTER_MOTION_MASK)
        self.canvas.add_events(Gdk.EventMask.BUTTON_RELEASE_MASK)
        self.canvas.connect("button-press-event", self.on_canvas_click)
        self.canvas.connect("motion-notify-event", self.on_canvas_motion)
        self.canvas.connect("button-release-event", self.on_canvas_release)
        ### canvas click state
        self.canvas_is_clicked = False

        ## mouse scroll
        self.canvas.add_events(Gdk.EventMask.SCROLL_MASK)
        self.canvas.connect("scroll-event", self.on_scroll)

        ## draw update
        self.canvas.connect("draw", self.update_simulation_)

        def draw_tick() -> bool:  # Canvas update
            self.canvas.queue_draw()
            return True

        GLib.timeout_add(60, draw_tick)

        # Simulation UI
        self.sliding_window_entry: Gtk.Entry = self.builder.get_object(
            "sliding_window_entry"
        )
        self.sliding_window_entry.connect(
            "changed", self.on_sliding_window_entry_changed
        )

        # Filters / Lenses UI

        ## Filter add button
        self.filter_add_button: Gtk.Button = self.builder.get_object(
            "filter_add_button"
        )
        self.filter_add_button.connect("clicked", self.add_filter)

        ## filters box
        self.filters_box: Gtk.Box = self.builder.get_object("filters_box")

        ## Lens select button
        self.lens_select_button: Gtk.Button = self.builder.get_object(
            "lens_select_button"
        )
        self.lens_select_button.connect("clicked", self.select_lens)

        ## lenses box
        self.lens_box = self.builder.get_object("lens_box")

        # Flow Control UI

        ## Slider
        self.progression_slider: Gtk.Scale = self.builder.get_object(
            "progression_slider"
        )
        self.progression_slider.connect(
            "change-value", self.on_progression_slider_value_change
        )

        ## Play / pause button
        self.play_pause_button: Gtk.Button = self.builder.get_object(
            "play_pause_button"
        )
        self.play_pause_button.connect("clicked", self.on_play_pause_click)

        self.camera = Camera(
            (-self.window.get_size().width // 2, -self.window.get_size().height // 2)
        )

        # CSS
        try:
            self.css_provider = Gtk.CssProvider()
            with open("style.css") as f:
                self.css_provider.load_from_data(f.read().encode())
            context = Gtk.StyleContext()
            screen = Gdk.Screen.get_default()
            context.add_provider_for_screen(
                screen, self.css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
        except Exception:
            print("[warning] could not load CSS")
            print(traceback.format_exc())

        self.cur_analyse_path: Optional[str] = None
        self.simulation: Optional[Simulation] = None
        self.last_update_time: Optional[float] = None

        self.window.show_all()

    def on_open_file(self, *args):
        file_chooser = Gtk.FileChooserDialog(
            title="Open...", parent=self.window, action=Gtk.FileChooserAction.OPEN
        )
        file_chooser.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )
        file_chooser.show()

        response = file_chooser.run()

        if response == Gtk.ResponseType.OK:
            self.path = file_chooser.get_filename()

            try:
                self.simulation = Simulation(Analyse.from_file(self.path))
            except Exception:
                print(f"[error] could not open {self.path}")
                file_chooser.destroy()
                return

            self.last_update_time = None

            # clean all filters ui if needed
            for filter_ui in self.filters_box:
                self.filters_box.remove(filter_ui)

            # refresh progression slider marks
            self.refresh_progression_slider_marks()

            # put play/pause button in the correct state
            self.play_pause_button.set_label(
                "\u23f5" if self.simulation.analyse_is_paused else "\u23f8"
            )

        file_chooser.destroy()

    def on_scroll(self, widget, event):
        self.camera.zoom *= 2 if event.direction == Gdk.ScrollDirection.UP else 0.5

    def on_play_pause_click(self, *args):
        self.simulation.toggle_analyse_pause_()
        self.play_pause_button.set_label(
            "\u23f5" if self.simulation.analyse_is_paused else "\u23f8"
        )

    def on_sliding_window_entry_changed(self, entry: Gtk.Entry, *args):

        if self.simulation is None:
            return

        entry_text = entry.get_text()
        try:
            entry_text_as_int = int(entry_text)
        except Exception:
            self.simulation.set_sliding_window_(None)
            return

        if entry_text_as_int <= 0 or entry_text_as_int >= len(
            self.simulation.analyse.steps
        ):
            self.simulation.set_sliding_window_(None)
            return

        self.simulation.set_sliding_window_(entry_text_as_int)

    def on_progression_slider_value_change(
        self, slider: Gtk.Scale, scroll_type: Gtk.ScrollType, value: float
    ):
        if not scroll_type == Gtk.ScrollType.JUMP:
            return

        if self.simulation is None:
            return

        # keep percentage between 0 and 1
        progression_percentage = max(0, min(value / 100, 1))
        interaction_idx = int(
            progression_percentage * (len(self.simulation.analyse.steps) - 1)
        )

        if interaction_idx == self.simulation.cur_step_idx:
            return

        if interaction_idx < self.simulation.cur_step_idx:
            for _ in range(self.simulation.cur_step_idx - interaction_idx):
                self.simulation.analyse_backward_()
        else:
            for _ in range(interaction_idx - self.simulation.cur_step_idx):
                self.simulation.analyse_forward_()
        self.simulation.update_positions_()

    def on_canvas_click(self, canvas: Gtk.DrawingArea, event: Gdk.EventButton):
        self.canvas_is_clicked = True
        self.canvas_click_position = (event.x, event.y)

    def on_canvas_motion(self, canvas: Gtk.DrawingArea, event: Gdk.EventMotion):
        if not self.canvas_is_clicked:
            return
        self.camera.add_offset(
            -(event.x - self.canvas_click_position[0]),
            -(event.y - self.canvas_click_position[1]),
        )
        self.canvas_click_position = (event.x, event.y)

    def on_canvas_release(self, canvas: Gtk.DrawingArea, event: Gdk.EventButton):
        self.canvas_is_clicked = False

    def add_filter(self, *args):

        # if no simulation is active, it's pointless to add a filter to it
        if self.simulation is None:
            return

        dialog = SelectFromListDialog(self.window, all_filters, lambda fltr: fltr.name)
        answer = dialog.run()

        if answer == Gtk.ResponseType.OK:

            if not dialog.current_selection is None:

                # get a filter from the dialog window
                fltr: Filter = dialog.current_selection()

                # add it to the simulation
                self.simulation.filters.append(fltr)

                # the UI for a single filter is as follows :
                # VBOX [
                #   HBOX [ [remove filter button] [invert switch] [title] ]
                #   GRID 2xn [
                #     [parameter name] [parameter entry widget]
                #     ... (n times)
                #   ]
                # ]

                ## header box : [remove filter button] [invert switch] [title]
                header_box = Gtk.Box()
                remove_button = Gtk.Button(label="-")
                remove_button.connect(
                    "clicked", self.remove_filter, len(self.filters_box.get_children())
                )
                invert_switch = Gtk.Switch()

                def invert_filter(widget, activated: bool, fltr: Filter):
                    fltr.inverted = activated

                invert_switch.connect("state-set", invert_filter, fltr)
                header_box.add(remove_button)
                header_box.add(invert_switch)
                header_box.add(Gtk.Label(label=fltr.__class__.__name__))

                ## filter box :
                ## [header box]
                ## [parameters grid]
                filter_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
                filter_box.add(header_box)
                filter_box.add(fltr.parameters_ui())

                # add the filter box to the filters box
                self.filters_box.add(filter_box)

                # refresh the filters box
                self.filters_box.show_all()

        dialog.destroy()

    def remove_filter(self, widget, filter_idx: int):
        """
        Remove the filter at index ``filter_idx``
        """

        if self.simulation is None:
            return

        self.simulation.filters.remove(self.simulation.filters[filter_idx])

        # a bit of a hack
        for i, filter_ui in enumerate(self.filters_box.get_children()):
            if i == filter_idx:
                self.filters_box.remove(filter_ui)

    def select_lens(self, *args):
        dialog = SelectFromListDialog(self.window, all_lenses, lambda lens: lens.name)
        answer = dialog.run()

        if answer == Gtk.ResponseType.OK:

            if not dialog.current_selection is None:

                # get a lens from the dialog window
                lens: Lens = dialog.current_selection()

                # set the camera lens
                self.camera.lens = lens

                # empty the lens box
                for widget in self.lens_box.get_children():
                    self.lens_box.remove(widget)

                # A VBOX for the lens
                # VBOX [
                #   [title]
                #   GRID 2xn [
                #     [parameter name] [parameter entry area]
                #     ... (n times)
                #   ]
                # ]
                self.lens_box.add(Gtk.Label(label=lens.__class__.__name__))
                self.lens_box.add(lens.parameters_ui())

                # refresh the lens ui vbox
                self.lens_box.show_all()

        dialog.destroy()

    def refresh_progression_slider_marks(self):
        """Sets mark on the progression slider. Two types of marks are set :
        - events marks, at the top
        - sliding window marks, at the bottom
        """
        self.progression_slider.clear_marks()

        if self.simulation is None:
            return

        # events marks
        for idx, step in enumerate(self.simulation.analyse.steps):
            if step.__class__ != EventStep:
                continue
            self.progression_slider.add_mark(
                clamp(idx * 100 / len(self.simulation.analyse.steps), 100),
                Gtk.PositionType.TOP,
                step.name,
            )

        # sliding window mark
        sliding_window_idx = 0
        if not self.simulation.sliding_window is None:
            sliding_window_idx = (
                self.simulation.cur_step_idx - self.simulation.sliding_window
            )
        self.progression_slider.add_mark(
            sliding_window_idx * 100 / len(self.simulation.analyse.steps),
            Gtk.PositionType.BOTTOM,
            None,
        )

    def update_simulation_(self, canvas, cairo_context):

        if self.simulation is None:
            return

        # update simulation
        ## self.last_update_time must be set to None the first iteration
        self.simulation.update_()
        self.last_update_time = time.time()

        # draw
        self.camera.draw(self.simulation, cairo_context)

        # update UI
        # update slider
        progression = self.simulation.progression()
        if not progression is None:
            self.progression_slider.set_value(clamp(progression * 100, 100))
        self.refresh_progression_slider_marks()


pyrat = PyRAT()
Gtk.main()
