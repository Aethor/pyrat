import json


stdin_raw_input = open(0).read()
stdin_input = json.loads(stdin_raw_input)

out_dict = {}


# characters
out_dict["characters"] = []
for character_dict in stdin_input["characters"]:
    new_character_dict = {
        "interactions_count": character_dict["interactionsCount"],
        "name": character_dict["name"],
        "tags": [],
    }
    out_dict["characters"].append(new_character_dict)


# conversion interaction => steps
out_dict["steps"] = []
for interaction in stdin_input["interactions"]:
    for target_character in interaction["targetCharacters"]:
        step = {
            "type": "interaction",
            "characters": [interaction["activeCharacter"], target_character["name"]],
            "polarity": target_character["influence"],
        }
        out_dict["steps"].append(step)


print(json.dumps(out_dict, indent=4))
