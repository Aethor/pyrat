from typing import Callable, List, Type, Optional, TypeVar
from abc import ABC

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class UIParameter(ABC):
    def __init__(self, name: str, *args, **kwargs) -> None:
        self.name = name
        self.widget = Gtk.Widget()


class BoolUIParameter(UIParameter):
    def __init__(self, name: str) -> None:
        self.name = name
        self.widget = Gtk.CheckButton()


class IntUIParameter(UIParameter):
    def __init__(self, name: str, on_change: Callable[[int], None]) -> None:
        self.name = name

        self.widget = Gtk.Entry()

        def local_on_change(*args):
            try:
                value = int(self.widget.get_text())
            except Exception:
                return
            on_change(value)

        self.widget.connect("changed", local_on_change)


class StringUIParameter(UIParameter):
    def __init__(self, name: str, on_change: Callable[[str], None]):
        self.name = name

        self.widget = Gtk.Entry()

        def local_on_change(*args):
            on_change(self.widget.get_text())

        self.widget.connect("changed", local_on_change)


class RangeUIParameter(UIParameter):
    def __init__(self, name: str, min_value: float, max_value: float) -> None:
        self.name = name
        self.min_value = min_value
        self.max_value = max_value

        adjustment = Gtk.Adjustment(lower=self.min_value, upper=self.max_value)
        self.widget = Gtk.Scale(adjustment=adjustment)


class ListSelectParameter(UIParameter):
    """
    An UI Parameter that can be used to select a value in a list of string
    """

    def __init__(
        self, name: str, selection_list: List[str], on_change: Callable[[str], None]
    ):
        self.name = name

        def local_on_change(button: Gtk.RadioButton, elem: str):
            if not button.get_active():
                return
            on_change(elem)

        self.widget = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        for elem_idx, elem in enumerate(selection_list):

            # first element is created differently to create the radio button group
            if elem_idx == 0:
                first_button = Gtk.RadioButton.new_with_label_from_widget(None, elem)
                button = first_button
            else:
                button = Gtk.RadioButton.new_from_widget(first_button)
                button.set_label(elem)

            button.connect("toggled", local_on_change, elem)
            self.widget.add(button)


def ui_from_parameters(ui_parameters: List[UIParameter]) -> Gtk.Grid:
    """
    :param parent:
    :param parameters:
    
    :return: a Gtk grid, with 2 columns and ``len(ui_parameters)`` rows
    """
    grid = Gtk.Grid()
    grid.set_row_homogeneous(True)
    grid.set_column_homogeneous(True)

    for i, ui_parameter in enumerate(ui_parameters):
        grid.attach(Gtk.Label(ui_parameter.name), 0, i, 1, 1)
        grid.attach(ui_parameter.widget, 1, i, 1, 1)

    return grid


T = TypeVar("T")


class SelectFromListDialog(Gtk.Dialog):
    """
    A dialog used to select an element from a list 
    
    :note: inspired by https://python-gtk-3-tutorial.readthedocs.io/en/latest/dialogs.html
    :var current_selection: the class of the currently selected element, or None if len()
    """

    def __init__(
        self,
        parent: Gtk.Window,
        selection_list: List[T],
        elem_to_name: Optional[Callable[[T], str]],
    ) -> None:
        """
        :param parent: parent window
        :param selection_list: list to select from
        :elem_to_name: a function taking an element from the list, and returning a displayable name. 
        """

        Gtk.Dialog.__init__(self, title="Select filter", transient_for=parent)

        self.add_buttons(
            Gtk.STOCK_OK, Gtk.ResponseType.OK, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL
        )

        self.current_selection: Optional[Type] = None

        if len(selection_list) == 0:
            return

        self.selection_list = selection_list
        self.current_selection = self.selection_list[0]

        main_box: Gtk.Box = self.get_content_area()

        # Filters radio buttons
        for elem_idx, elem in enumerate(self.selection_list):

            # first element is created differently to create the radio button group
            if elem_idx == 0:
                first_button = Gtk.RadioButton.new_with_label_from_widget(
                    None, elem_to_name(self.selection_list[0])
                )
                button = first_button
            else:
                button = Gtk.RadioButton.new_from_widget(first_button)
                button.set_label(elem_to_name(elem))

            button.connect("toggled", self.on_button_toggled, elem_idx)
            main_box.pack_start(button, False, False, 0)

        self.show_all()

    def on_button_toggled(self, button, elem_idx: int):
        if not button.get_active():
            return
        self.current_selection = self.selection_list[elem_idx]
