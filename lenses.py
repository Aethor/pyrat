from abc import ABC
from typing import Callable, Dict, List, Mapping, Optional, Any

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from ui import IntUIParameter, ListSelectParameter, ui_from_parameters

from networkx.algorithms import centrality
from networkx.algorithms import isomorphism
from node2vec import Node2Vec

from simulation.simulation import Simulation
from colors import colors, hex_color_to_rgb, yellow_to_red_gradient_hex

from sklearn.cluster import KMeans
import networkx as nx


class Lens(ABC):
    """
    :var name: display name
    """

    name = None

    def apply(self, simulation: Simulation) -> Dict[str, str]:
        """
        :return: A dict with character names as keys and colors as values
        """
        raise NotImplemented

    def parameters_ui(self) -> Gtk.Grid:
        raise NotImplemented


class SpatialKMeansLens(Lens):

    name = "spatial kmeans"

    def __init__(self, k: int = 2) -> None:
        assert k < len(colors)
        self.set_k(k)

    def apply(self, simulation: Simulation) -> Dict[str, str]:

        characters = simulation.filtered_characters()

        # special case where we have more clusters than characters
        if self.k > len(characters):
            print(f"{self.__class__} warning : k > len(characters)")
            return {character.name: "#000000" for character in characters}

        # perform clustering
        kmeans = KMeans(n_clusters=self.k)
        X = [character.position for character in characters]
        kmeans.fit_transform(X)
        labels = list(kmeans.labels_)

        # assign character to clusters
        clusters: List[List[str]] = [[] for _ in range(len(set(labels)))]
        for label, character in zip(labels, characters):
            clusters[label - 1].append(character.name)
        for cluster in clusters:
            cluster = sorted(cluster)
        clusters = sorted(clusters, key=lambda e: e[0])

        # create character to color dict
        character_to_color: Dict[str, str] = {}
        for idx, cluster in enumerate(clusters):
            for character in cluster:
                character_to_color[character] = colors[idx]

        return character_to_color

    def parameters_ui(self) -> Gtk.Grid:
        return ui_from_parameters([IntUIParameter("k", self.set_k)])

    def set_k(self, k: int):
        if k <= 0:
            self.k = 1
            return
        self.k = k


class Node2VecKMeanLens(Lens):

    name = "node2vec + kmeans"

    MIN_PROB = 0.01

    def __init__(self, k: int = 2) -> None:
        self.k = k
        self.cache: Optional[Dict[str, Any]] = None
        self.cache_value: Optional[Dict[str, str]] = None

    def apply(self, simulation: Simulation) -> Dict[str, str]:
        graph = simulation.filtered_graph()
        characters = simulation.filtered_characters()

        if len(graph.nodes) == 0 or len(graph.edges) == 0:
            return {}

        cache = {"graph": graph, "k": self.k}
        if self.cache_equals(cache):
            # by logic this is never None
            return self.cache_value  # type: ignore
        self.cache = cache

        # negative probabilities can't exist in n2v
        for u, v, d in graph.edges(data=True):
            if d["weight"] < Node2VecKMeanLens.MIN_PROB:
                d["weight"] = Node2VecKMeanLens.MIN_PROB

        n2v = Node2Vec(graph, quiet=True)
        model = n2v.fit()

        kmeans = KMeans(n_clusters=min(self.k, len(characters)))
        X = [model.wv[character.name] for character in characters]
        kmeans.fit_transform(X)
        labels = list(kmeans.labels_)

        # assign character to clusters
        clusters: List[List[str]] = [[] for _ in range(len(set(labels)))]
        for label, character in zip(labels, characters):
            clusters[label - 1].append(character.name)
        for cluster in clusters:
            cluster = sorted(cluster)
        clusters = sorted(clusters, key=lambda e: e[0])

        character_to_color: Dict[str, str] = {}
        for idx, cluster in enumerate(clusters):
            for character in cluster:
                character_to_color[character] = colors[idx]

        self.cache_value = character_to_color
        return character_to_color

    def parameters_ui(self) -> Gtk.Grid:
        return ui_from_parameters([IntUIParameter("k", self.set_k)])

    def cache_equals(self, cache: Dict[str, Any]):
        if self.cache is None:
            return False
        if cache["k"] != self.cache["k"]:
            return False
        return nx.is_isomorphic(
            self.cache["graph"],
            cache["graph"],
            edge_match=isomorphism.numerical_edge_match("weight", 1),
        )

    def set_k(self, k: int):
        if k <= 0:
            self.k = 1
            return
        self.k = k


def degree_centrality(graph: nx.Graph) -> Dict[str, float]:
    return {node: degree for (node, degree) in graph.degree}


class CentralityLens(Lens):

    name = "centrality"

    defaultCentralityMeasure = "betweenness"

    centrality_fns: Mapping[str, Callable[[nx.Graph], Dict[str, float]]] = {
        "betweenness": nx.betweenness_centrality,
        "harmonic": nx.harmonic_centrality,
        "degree": nx.degree_centrality,
    }

    def __init__(self, centrality_measure: str = "betweenness") -> None:
        assert centrality_measure in CentralityLens.centrality_fns
        self.centrality_fn = CentralityLens.centrality_fns[centrality_measure]

    def apply(self, simulation: Simulation) -> Dict[str, str]:

        graph = simulation.filtered_graph()

        character_to_centrality = self.centrality_fn(graph)

        if len(character_to_centrality) == 0:
            return {}

        max_centrality = max(character_to_centrality.values())
        min_centrality = min(character_to_centrality.values())

        range_ratio = max_centrality - min_centrality
        if range_ratio == 0:
            return {
                character.name: "#000000"
                for character in simulation.filtered_characters()
            }

        character_to_relative_centrality = {
            character: (centrality - min_centrality) / (max_centrality - min_centrality)
            for character, centrality in character_to_centrality.items()
        }

        character_to_color = {
            character: yellow_to_red_gradient_hex(relative_centrality)
            for character, relative_centrality in character_to_relative_centrality.items()
        }

        return character_to_color

    def parameters_ui(self) -> Gtk.Grid:
        return ui_from_parameters(
            [
                ListSelectParameter(
                    "centrality measure",
                    list(CentralityLens.centrality_fns.keys()),
                    self.set_centrality_measure,
                )
            ]
        )

    def set_centrality_measure(self, centrality_measure: str):
        if not centrality_measure in CentralityLens.centrality_fns:
            self.centrality_fn = CentralityLens.centrality_fns[
                CentralityLens.defaultCentralityMeasure
            ]
            return
        self.centrality_fn = CentralityLens.centrality_fns[centrality_measure]


all_lenses = [SpatialKMeansLens, Node2VecKMeanLens, CentralityLens]
