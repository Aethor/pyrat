.. PyRAT documentation master file, created by
   sphinx-quickstart on Sat Nov 28 16:00:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyRAT's documentation!
=================================


Installation
============

- `pip install -r requirements.txt` will install the required python packages
- `Gtk` and `PyGObject` are needed. See instructions here for installation : https://pygobject.readthedocs.io/en/latest/getting_started.html
- Launch the software with `python3 pyrat.py`


Simulation
==========

.. automodule:: simulation.simulation
   :members:


Characters
----------

.. automodule:: simulation.characters
   :members:


Lenses
======

Lenses are ways of seeing the current graph differently. Only one lens may be activated at a time.

.. automodule:: lenses
   :members:


Filters
=======

`Filters` can narrow the graph down to a certain set of characters. Put simply, they are functions taking a set of characters and returning of the subset of the input set. As many filters as you want may be activated at the same time. Each filter can also be inverted.

.. automodule:: filters
   :members:
