from __future__ import annotations
from typing import Optional, List, Set

import numpy as np


class Character:
    """class representing a character

    :ivar name: the character's name
    :ivar interactions_count: the total number of
        interactions of the character
    :ivar cur_interactions_count: the current number
        of interactions of the character in a simulation
    :ivar tags: a list of character tags, that can be used
        for grouping.
    """

    def __init__(self, name: str, interactions_count: int, tags: List[str]):

        self.name = name
        self.interactions_count = interactions_count
        self.cur_interactions_count = 0
        self.tags = tags
        self.position: Optional[np.array] = None
        self.relations: Set[Relation] = set()

    def __eq__(self, o: Character) -> bool:
        """equality test between two characters

        :param o:
        """
        return o.name == self.name

    def __hash__(self) -> int:
        """hashing of a character, based on its name"""
        return hash(self.name)

    def __repr__(self) -> str:
        return f"<Character({self.name}, {self.cur_interactions_count}/{self.interactions_count}, {self.tags})>"

    def relation_with(self, other_character: Character) -> Optional[Relation]:
        """
        Returns the character relation with ``other_character``, or None if this
        relation does not exists

        :param other_character:
        """
        for relation in self.relations:
            if relation.has_character(other_character.name):
                return relation
        return None


class Relation:
    """A relation between two characters"""

    def __init__(
        self,
        character1: Character,
        character2: Character,
        polarity: float = 0,
        cur_interactions_count: int = 0,
    ):

        assert character1 != character2
        assert cur_interactions_count >= 0

        self.character1 = character1
        self.character2 = character2
        self.polarity = polarity
        self.cur_interactions_count = cur_interactions_count

    def __eq__(self, o: Relation) -> bool:
        return o.has_character(self.character1.name) and o.has_character(
            self.character2.name
        )

    def __hash__(self) -> int:
        return hash(frozenset((self.character1, self.character2)))

    def __repr__(self) -> str:
        return f"<Relation({self.character1.name}, {self.character2.name}, {self.polarity}, {self.cur_interactions_count})>"

    def is_positive(self) -> bool:
        """Check if a relation is positive, regarding its polarity"""
        return self.polarity > 0

    def has_character(self, character_name: str) -> bool:
        """check if the input character is part of the relation

        :param character_name: the name of the character
        :todo: replace input name by a ``Character``
        """
        return (
            self.character1.name == character_name
            or self.character2.name == character_name
        )

    def other_character(self, character: Character) -> Character:
        """Returns the other character of a relationships
        :param character: ``self.has_character(character)`` must be ``True``
        """
        if character == self.character1:
            return self.character2
        if character == self.character2:
            return self.character1
        assert False
