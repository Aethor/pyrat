from __future__ import annotations
import random, json
from typing import Dict, List, Optional, Tuple, Any, Set

from simulation.characters import Character, Relation
import filters

import numpy as np
import networkx as nx


class Step:
    """
    A Simulation step, that can go both forward and backwards
    """

    @classmethod
    def from_dict(
        cls, input_dict: Dict[str, Any], characters: Dict[str, Character]
    ) -> Step:
        """Parse a dictionary to create a ``Step``

        :param cls:
        :param input_dict:
        :param characters:
        :returns:
        """
        raise NotImplemented

    def to_dict(self) -> Dict[str, Any]:
        """Convert the step to a dictionary that can be serialized later"""
        raise NotImplemented

    def apply_forward(self, simulation: Simulation) -> Simulation:
        """Apply the step to a simulation

        :param simulation:
        :returns:
        """
        raise NotImplemented

    def apply_backward(self, simulation: Simulation) -> Simulation:
        """Unapply the step to a simulation

        :param simulation:
        :returns:
        """
        raise NotImplemented


class HideStep(Step):
    def __init__(self, character: Character) -> None:
        self.character = character

    def to_dict(self) -> Dict[str, Any]:
        return {
            "type": step_class_to_name[self.__class__],
            "character": self.character.name,
        }

    @classmethod
    def from_dict(
        cls, input_dict: Dict[str, Any], characters: Dict[str, Character]
    ) -> HideStep:
        return HideStep(characters[input_dict["character"]])

    def apply_forward(self, simulation: Simulation) -> Simulation:
        simulation.hidden_characters.add(self.character)
        return simulation

    def apply_backward(self, simulation: Simulation) -> Simulation:
        simulation.hidden_characters.remove(self.character)
        return simulation


class ShowStep(Step):
    def __init__(self, character: Character) -> None:
        self.character = character

    def to_dict(self) -> Dict[str, Any]:
        return {
            "type": step_class_to_name[self.__class__],
            "character": self.character.name,
        }

    @classmethod
    def from_dict(
        cls, input_dict: Dict[str, Any], characters: Dict[str, Character]
    ) -> ShowStep:
        return ShowStep(characters[input_dict["character"]])

    def apply_forward(self, simulation: Simulation) -> Simulation:
        simulation.hidden_characters.remove(self.character)
        return simulation

    def apply_backward(self, simulation: Simulation) -> Simulation:
        simulation.hidden_characters.add(self.character)
        return simulation


class EventStep(Step):
    def __init__(self, name: str) -> None:
        self.name = name

    def to_dict(self) -> Dict[str, Any]:
        return {"type": step_class_to_name[self.__class__], "name": self.name}

    @classmethod
    def from_dict(
        cls, input_dict: Dict[str, Any], characters: Dict[str, Character]
    ) -> EventStep:
        return EventStep(input_dict["name"])

    def apply_forward(self, simulation: Simulation) -> Simulation:
        return simulation

    def apply_backward(self, simulation: Simulation) -> Simulation:
        return simulation


class InteractionStep(Step):
    def __init__(
        self, characters: Tuple[Character, Character], polarity: float
    ) -> None:
        self.characters = characters
        self.polarity = polarity

    def to_dict(self) -> Dict[str, Any]:
        return {
            "type": step_class_to_name[self.__class__],
            "characters": [c.name for c in self.characters],
            "polarity": self.polarity,
        }

    @classmethod
    def from_dict(
        cls, input_dict: Dict[str, Any], characters: Dict[str, Character]
    ) -> InteractionStep:
        return InteractionStep(
            (
                characters[input_dict["characters"][0]],
                characters[input_dict["characters"][1]],
            ),
            input_dict["polarity"],
        )

    def apply_forward(self, simulation: Simulation) -> Simulation:

        for character in self.characters:

            character.cur_interactions_count += 1

            # add character to the simulation if it doesnt exist yet
            if not character in simulation.characters:
                character.position = np.array(
                    [
                        300 * (random.random() - 0.5),
                        300 * (random.random() - 0.5),
                    ]
                )
                simulation.characters.append(character)

        # relation update
        relation = simulation.relation(self.characters[0], self.characters[1])
        ## add relation if it doesn exist
        if relation is None:
            new_relation = Relation(
                self.characters[0],
                self.characters[1],
                self.polarity,
                1,
            )
            simulation.relations.add(new_relation)
            self.characters[0].relations.add(new_relation)
            self.characters[1].relations.add(new_relation)
        else:
            relation.polarity += self.polarity
            relation.cur_interactions_count += 1

        return simulation

    def apply_backward(self, simulation: Simulation) -> Simulation:

        for character in self.characters:
            character.cur_interactions_count -= 1

        relation = simulation.relation(self.characters[0], self.characters[1])

        if relation is None:
            print(
                f"warning : trying to remove unexisting relation between {self.characters[0]} and {self.characters[1]}. Aborting..."
            )
            return simulation

        relation.polarity -= self.polarity
        relation.cur_interactions_count -= 1

        # If the relation has no more interactions, we remove it
        if relation.cur_interactions_count == 0:
            simulation.relations.remove(relation)
            self.characters[0].relations.remove(relation)
            self.characters[1].relations.remove(relation)

        # remove character if needed
        for character in self.characters:
            if character.cur_interactions_count == 0:
                simulation.characters.remove(character)

        return simulation


class FusionStep(Step):
    def __init__(
        self, source_characters: List[Character], target_character: Character
    ) -> None:
        self.source_characters = source_characters
        self.target_character = target_character

    def to_dict(self) -> Dict[str, Any]:
        return {
            "type": step_class_to_name[self.__class__],
            "source_characters": [c.name for c in self.source_characters],
            "target_character": self.target_character.name,
        }

    @classmethod
    def from_dict(
        cls, input_dict: Dict[str, Any], characters: Dict[str, Character]
    ) -> FusionStep:
        return FusionStep(
            [characters[name] for name in input_dict["source_characters"]],
            characters[input_dict["target_character"]],
        )

    def apply_forward(self, simulation: Simulation) -> Simulation:
        # add source characters total interactions count to target
        self.target_character.interactions_count += sum(
            [c.interactions_count for c in self.source_characters]
        )

        # add source characters current interactions count to target
        self.target_character.cur_interactions_count += sum(
            [c.cur_interactions_count for c in self.source_characters]
        )

        # fuse source characters relations into target
        for source_character in self.source_characters:

            for source_relation in source_character.relations:

                other_character = source_relation.other_character(source_character)

                # dont create a relation between a character and himself !
                if other_character == self.target_character:
                    continue

                target_relation = self.target_character.relation_with(other_character)

                # relation does not exit for target character : create it
                if target_relation is None:
                    target_relation = Relation(self.target_character, other_character)
                    simulation.relations.add(target_relation)
                    self.target_character.relations.add(target_relation)
                    other_character.relations.add(target_relation)

                # update relation
                target_relation.cur_interactions_count += (
                    source_relation.cur_interactions_count
                )
                target_relation.polarity += source_relation.polarity

        return simulation

    def apply_backward(self, simulation: Simulation) -> Simulation:
        # remove source characters total interactions count from target
        self.target_character.interactions_count -= sum(
            [c.interactions_count for c in self.source_characters]
        )

        # remove source characters current interactions from target
        self.target_character.cur_interactions_count -= sum(
            [c.cur_interactions_count for c in self.source_characters]
        )

        # remove source characters relations from target characters relation
        for source_character in self.source_characters:

            for source_relation in source_character.relations:

                other_character = source_relation.other_character(source_character)

                if other_character == self.target_character:
                    continue

                target_relation = self.target_character.relation_with(other_character)

                if target_relation is None:
                    print(
                        f"warning : trying to remove unexisting relation between {self.target_character} and {other_character}. Aborting..."
                    )
                    continue

                target_relation.cur_interactions_count -= (
                    source_relation.cur_interactions_count
                )
                target_relation.polarity -= source_relation.polarity

                # relation is now empty : delete it
                if target_relation.cur_interactions_count == 0:
                    simulation.relations.remove(target_relation)
                    self.target_character.relations.remove(target_relation)
                    other_character.relations.remove(target_relation)

        return simulation


#: mapping of step name in input json file to step class
step_name_to_class = {
    "interaction": InteractionStep,
    "fusion": FusionStep,
    "hide": HideStep,
    "show": ShowStep,
    "event": EventStep,
}
step_class_to_name = {v: k for k, v in step_name_to_class.items()}


class Analyse:
    def __init__(self, characters: Dict[str, Character], steps: List[Step]):
        self.characters = characters
        self.steps = steps

    @classmethod
    def from_json(cls, json_string: str) -> Analyse:
        analyse_dict = json.loads(json_string)

        characters = {
            character["name"]: Character(
                character["name"], character["interactions_count"], character["tags"]
            )
            for character in analyse_dict["characters"]
        }

        steps: List[Step] = []
        for step_dict in analyse_dict["steps"]:
            step_type = step_dict["type"]
            if not step_type in step_name_to_class:
                print(f"[warning] unkown step type name : {step_type}. Ignoring...")
                continue
            step_class = step_name_to_class[step_type]
            step = step_class.from_dict(step_dict, characters)
            steps.append(step)

        return Analyse(characters, steps)

    @classmethod
    def from_file(cls, path: str) -> Analyse:
        return Analyse.from_json(open(path).read())

    def to_json(self, **kwargs) -> str:
        """Convert the Analyse back to a json string"""

        characters = [
            {
                "name": character.name,
                "interactions_count": character.interactions_count,
                "tags": character.tags,
            }
            for character in self.characters.values()
        ]

        steps = [step.to_dict() for step in self.steps]

        return json.dumps({"characters": characters, "steps": steps}, **kwargs)

    def to_file(self, path, **kwargs):
        json_string = self.to_json(**kwargs)
        with open(path, "w") as f:
            f.write(json_string)


class Simulation:
    """
    :var sliding_window: ``[---(--|---]``

    :var characters: the list of characters currently in the simulation

    :var analyse_is_paused: when True, self.update_() won't process through ``self.analyse``
    """

    def __init__(self, analyse: Analyse) -> None:
        self.analyse = analyse
        self.cur_step_idx: Optional[int] = None
        self.sliding_window: Optional[int] = None

        self.characters: List[Character] = []
        self.relations: Set[Relation] = set()

        self.hidden_characters: Set[Character] = set()

        self.analyse_is_paused = False

        self.filters: List[filters.Filter] = []

        # {character_name:position}
        self.character_positions: Optional[Dict[str, np.array]] = None

    def filtered_characters(self) -> List[Character]:
        filtered_characters = self.characters
        for filter in self.filters:
            filtered_characters = filter.apply(filtered_characters)
        filtered_characters = [
            c for c in filtered_characters if not c in self.hidden_characters
        ]
        return filtered_characters

    def filtered_relations(self) -> List[Relation]:
        filtered_characters = self.filtered_characters()
        filtered_relations = [
            relation
            for relation in self.relations
            if relation.character1 in filtered_characters
            and relation.character2 in filtered_characters
        ]
        return filtered_relations

    def relation(
        self, character1: Character, character2: Character
    ) -> Optional[Relation]:
        for relation in self.relations:
            if relation.has_character(character1.name) and relation.has_character(
                character2.name
            ):
                return relation
        return None

    def progression(self) -> Optional[float]:
        if self.cur_step_idx is None:
            return None
        return self.cur_step_idx / len(self.analyse.steps)

    def filtered_graph(self) -> nx.Graph:
        graph = nx.Graph()
        graph.add_nodes_from(
            [
                (character.name, vars(character))
                for character in self.filtered_characters()
            ]
        )
        for relation in self.filtered_relations():
            graph.add_edge(
                relation.character1.name,
                relation.character2.name,
                weight=relation.polarity,
            )
        return graph

    def set_sliding_window_(self, sliding_window: Optional[int]):

        if self.sliding_window == sliding_window:
            return

        if sliding_window is None:

            if self.sliding_window is None:
                return

        assert sliding_window is None or sliding_window > 0

        cur_edge_idx = (
            self.cur_step_idx - self.sliding_window + 1
            if not self.sliding_window is None
            else 0
        )
        new_edge_idx = (
            self.cur_step_idx - sliding_window + 1 if not sliding_window is None else 0
        )
        new_edge_idx = max(0, new_edge_idx)  # cant have an edge < 0

        if new_edge_idx > cur_edge_idx:
            for step_idx in range(cur_edge_idx, new_edge_idx):
                step = self.analyse.steps[step_idx]
                if step.__class__ == InteractionStep:
                    step.apply_backward(self)
        else:
            for step_idx in range(new_edge_idx, cur_edge_idx):
                step = self.analyse.steps[step_idx]
                if step.__class__ == InteractionStep:
                    step.apply_forward(self)

        self.sliding_window = sliding_window

    def toggle_analyse_pause_(self):
        """
        Toggle the analyse reading state
        """
        self.analyse_is_paused = not self.analyse_is_paused

    def analyse_forward_(self):
        """
        Process a step from an analyse
        """
        # if we are at the end of the simulation, it's no use getting forward !
        if self.cur_step_idx == len(self.analyse.steps) - 1:
            return

        if self.cur_step_idx is None:
            self.cur_step_idx = 0
        else:
            self.cur_step_idx += 1

        if not self.cur_step_idx < len(self.analyse.steps):
            return

        cur_step = self.analyse.steps[self.cur_step_idx]
        cur_step.apply_forward(self)

        # -- sliding window concerns
        if self.sliding_window is None:
            return

        if self.cur_step_idx - self.sliding_window < 0:
            return

        # remove the step at the left edge of the sliding window
        edge_step = self.analyse.steps[self.cur_step_idx - self.sliding_window]
        if edge_step.__class__ != InteractionStep:
            return
        edge_step.apply_backward(self)

    def analyse_backward_(self):
        """
        Removes the last step from the current analyse
        """
        assert not self.cur_step_idx is None

        # If we are at the first step we have nothing to do
        if self.cur_step_idx <= 0:
            return

        # Back in time 1 step
        cur_step = self.analyse.steps[self.cur_step_idx]
        cur_step.apply_backward(self)
        self.cur_step_idx -= 1

        # -- sliding window concern
        if self.sliding_window is None:
            return

        if self.cur_step_idx - self.sliding_window + 1 < 0:
            return

        # add the step at the left edge of the sliding window
        edge_step = self.analyse.steps[self.cur_step_idx - self.sliding_window + 1]
        if edge_step.__class__ != InteractionStep:
            return
        edge_step.apply_forward(self)

    def update_(self):
        if self.analyse_is_paused:
            return

        self.analyse_forward_()
        self.update_positions_()

    def update_positions_(self):

        filtered_graph = self.filtered_graph()
        if len(filtered_graph) == 0:
            return

        self.character_positions = nx.spring_layout(
            filtered_graph, pos=self.character_positions, k=1
        )

        if self.character_positions is None:
            return

        for character in self.characters:
            if character.name in self.character_positions:
                # TODO: correct zoom constant elsewhere
                character.position = 500 * self.character_positions[character.name]
