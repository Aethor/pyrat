import gi

from simulation.characters import Character
from typing import List, Set
from ui import ui_from_parameters, IntUIParameter, StringUIParameter

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Filter:
    """
    :var name: interface display name

    :ivar inverted:
    """

    name = None

    def __init__(self):
        self.inverted = False

    def apply(self, characters: List[Character]) -> List[Character]:
        """An abstraction over ``self._apply``

        :note: should *not* be overrided
        """
        filtered_characters = self._apply(characters)
        if self.inverted:
            # TODO: performance
            return list(set(characters) - set(filtered_characters))
        return filtered_characters

    def _apply(self, characters) -> List[Character]:
        """Filter a character list

        :note: should be overrided
        """
        raise Exception("should be overrided")

    def parameters_ui(self) -> Gtk.Grid:
        raise Exception("should be overrided")


class MinInteractionsCountFilter(Filter):
    """
    A filter used to remove characters that are under a certain interaction count threshold
    """

    name = "interactions count"

    def __init__(self, min_count: int = 10) -> None:
        super().__init__()
        self.set_min_count(min_count)

    def _apply(self, characters: List[Character]) -> List[Character]:
        return [
            character
            for character in characters
            if character.interactions_count >= self.min_count
        ]

    def parameters_ui(self) -> Gtk.Grid:
        return ui_from_parameters([IntUIParameter("min_count", self.set_min_count)])

    def set_min_count(self, min_count):
        if min_count < 0:
            self.min_count = 0
            return
        self.min_count = min_count


class TopKInteractionsCountFilter(Filter):
    """
    A filter that only keeps the top k characters in terms of interactions
    """

    name = "top k interactions count"

    def __init__(self, k: int = 10) -> None:
        super().__init__()
        self.set_k(k)

    def _apply(self, characters: List[Character]) -> List[Character]:
        return sorted(characters, key=lambda char: char.interactions_count)[
            len(characters) - self.k :
        ]

    def parameters_ui(self) -> Gtk.Grid:
        return ui_from_parameters([IntUIParameter("k", self.set_k)])

    def set_k(self, k):
        if k <= 0:
            self.k = 1
            return
        self.k = k


class ManualFilter(Filter):
    """
    A filter that only keeps character not in a list
    """

    name = "manual"

    def __init__(self):
        super().__init__()
        self.names: Set[str] = set()

    def _apply(self, characters: List[Character]) -> List[Character]:

        filtered_characters = []

        for character in characters:
            if character.name in self.names:
                continue
            filtered_characters.append(character)

        return filtered_characters

    def parameters_ui(self) -> Gtk.Grid:
        return ui_from_parameters([StringUIParameter("names", self.set_names)])

    def set_names(self, names: str):
        """
        :param names: names, separated by a comma (current limitation)
        """
        self.names = set(names.split(","))


all_filters = [MinInteractionsCountFilter, TopKInteractionsCountFilter, ManualFilter]
