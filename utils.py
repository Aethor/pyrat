def clamp(value: float, max_value: float) -> float:
    return min(value, max_value)
